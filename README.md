### Bring Postal code lookup

This module fills in localities on the client side based on results from Bring's postal code service. 
The Bring postal code service is free, but has a volume limit.

This module can sit on top of any form. Node, taxonomy, order in checkout, custom form, anything. 
Just configure a set of selectors for where the data is to be filled in, plus a form ID, and you are set to go. 
Since it is entirely javascript-driven, it does not validate server side.
